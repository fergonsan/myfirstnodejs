import { LitElement, html } from "lit-element";
import "../persona-ficha-listado/persona-ficha-listado.js"
import "../persona-form/persona-form.js"
import "./persona-maindm.js"

class PersonaMain extends LitElement{

    static get properties(){
        return{
            people: {type: Array},            
            showPersonForm: {type: Boolean},           
            maxYearsInCompanyFilter: {type:Number} 

        };
    }  

    constructor(){
        super();
        this.showPersonForm=false;
        this.people = []
        this.maxYearsInCompanyFilter=0;
    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" 
                        rel="stylesheet" 
                        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" 
                        crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.filter(person => person.yearsInCompany <= this.maxYearsInCompanyFilter).map(
                        person => html`<persona-ficha-listado
                            profile="${person.profile}" 
                            fname="${person.name}" 
                            yearsInCompany="${person.yearsInCompany}"
                            .photo="${person.photo}"
                            @delete-person="${this.deletePerson}"
                            @info-person="${this.infoPerson}">
                            </persona-ficha-listado>`
                    )}                
                </div>
            </div>
            <div class="row">
                <persona-form @persona-form-store="${this.personFormStore}" @persona-form-close="${this.personFormClose}" class="d-none border rounded border-primary" id="personForm"></persona-form>
            </div>
            <persona-maindm @initial-loaded-people="${this.loadPeople}"></persona-maindm>
        `;
    }
    
    updated(changedProperties){
        console.log("updated");
        if(changedProperties.has("showPersonForm")){
            console.log("ha cambiado el valor de la propiedad showPersonForm en persona-main");
            if (this.showPersonForm === true){
                this.showPersonfromData();
            }else{
                this.showPersonList();
            }
        }
        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-main");
            this.dispatchEvent(new CustomEvent("updated-people",{
                detail:{
                    people: this.people
                }
            }));
        }        
        if(changedProperties.has("maxYearsInCompanyFilter")){
            console.log("Ha cambiado el valor de la propiedad maxYearsInCompanyFilter en persona-main");
            console.log("se van a mostrar las personas cuya antigüedad máxima sea "+this.maxYearsInCompanyFilter+" años");
        }

        
    }
    loadPeople(e){
        console.log("loadPeople en persona-main");
        console.log(e.detail);

        this.people = e.detail.people;
    }

    loadPeople(e){
        console.log("loadPeople en persona-main");
        console.log(e.detail);

        this.people = e.detail.people;
    }

    personFormClose(){
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de persona");
        this.showPersonForm = false;
    }
    personFormStore(e){
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");
        console.log("La propiedad name de person vale "+e.detail.person.name);
        console.log("La propiedad profile de person vale "+e.detail.person.profile);
        console.log("La propiedad yearsInCompany de person vale "+e.detail.person.yearsInCompany);
        
        if (e.detail.editingPerson === true){
            console.log("Se va a actualizar la persona de nombre "+e.detail.person.name)
            //let indexOfPerson = 
            //    this.people.findIndex(
            //        person => person.name === e.detail.person.name
            //    );
            //    if (indexOfPerson >= 0){
            //        console.log("Persona encontrada");
            //        e.detail.person.photo = this.people[indexOfPerson].photo;
            //        this.people[indexOfPerson] = e.detail.person;
            //    }

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                    ? person = e.detail.person : person
            );

        }else{
            console.log("Se va a almacenar una persona nueva")
            this.people = [...this.people, e.detail.person];
        }
        console.log("Proceso terminado");        
        this.showPersonForm = false;
    }
    showPersonfromData(){
        console.log("showPersonfromData")
        console.log("Mostrando el formulario de persona")
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    }
    showPersonList(){
        console.log("showPersonList")
        console.log("Mostrando el listado de personas")
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }
    
    deletePerson(e){
        console.log("deletePerson en persona-main"); 
        console.log("Se va a borrar la persona de nombre: "+e.detail.name);
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );        
    }
    infoPerson(e){
        console.log("infoPerson");
        console.log("Se ha pedido más información de la persona "+e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let person = {}; //Este es el objeto que se enviará al formulario.
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;
        
        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
        
    }
    resetFilter(){
        this.people=this.peopleAux;
    }
}

customElements.define("persona-main", PersonaMain);
