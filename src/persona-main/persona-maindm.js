import { LitElement, html } from "lit-element";

class PersonaMainDm extends LitElement{

    static get properties(){
        return{
            people: {type: Array},            
        };
    }  

    constructor(){
        super();        
        this.people = [
            {
                name: "Patty Haydee",
                yearsInCompany: 10,
                photo: {
                    src: "./img/caras-07.png",
                    alt: "Ellen Ripley"
                },
                profile: "loren ipsum dolor sit amet."               
            },
            {
                name: "Oliver Atom",
                yearsInCompany: 5,
                photo: {
                    src: "./img/caras-01.png",
                    alt: "Oliver Aton"
                },
                profile: "loren ipsum dolor sit amet loren ipsum dolor sit amet.loren ipsum dolor sit amet.loren ipsum dolor sit amet.loren ipsum dolor sit amet.loren ipsum dolor sit amet."              
            },
            {
                name: "Benji Price",
                yearsInCompany: 3,
                photo: {
                    src: "./img/caras-09.png",
                    alt: "Benjy Price"
                },
                profile: "loren ipsum dolor sit amet, consecutor."                
            },
            {
                name: "Mark Lenders",
                yearsInCompany: 4,
                photo: {
                    src: "./img/latinoamericano.png",
                    alt: "Benjy Price"
                },
                profile: "loren ipsum dolor sit amet, ramecatador clarae losti lumbre."               
            },
            {
                name: "Bruce Harper",
                yearsInCompany: 5,
                photo: {
                    src: "./img/caras-11.png",
                    alt: "Benjy Price"
                },
                profile: "loren ipsum dolor sit."                
            }
        ]
    }

    updated(changedProperties) {
        console.log("updated en persona-main.dm");
        console.log(changedProperties);
        if (changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-main-dm");
            this.dispatchEvent(
                new CustomEvent ("initial-loaded-people", {
                    detail: {
                        people: this.people
                    }
            }));

        }
    }

    
}

customElements.define("persona-maindm", PersonaMainDm);
