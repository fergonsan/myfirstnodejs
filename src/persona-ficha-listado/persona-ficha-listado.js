import { LitElement, html } from "lit-element";

class PersonaFichaListado extends LitElement{

    static get properties(){
        return{
            fname: {type: String},
            yearsInCompany: {type: Number},
            photo: {type: Object},
            profile: {type: String}
        };
    }

    constructor(){
        super();
    }
    
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" 
                    rel="stylesheet" 
                    integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" 
                    crossorigin="anonymous">
            <div class="card h-100">                
                <img src="${this.photo.src}" height="400" width="124" alt="${this.photo.alt}"  class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${this.fname}</h5>
                    <p class="card-text">${this.profile}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">${this.yearsInCompany} años en la empresa</li>
                    </ul>
                </div>
                <div class="card-footer">
                    <button class="btn btn-danger col-5" @click="${this.deletePerson}"><strong>X</strong></button>
                    <button class="btn btn-info offset-1 col-5" @click="${this.moreInfo}"><strong>Info</strong></button>
                </div>                                                
            </div>
        `;
    }
    moreInfo(e){
        console.log("moreInfo");
        console.log("Se ha pedido más información de la persona "+this.fname);
        this.dispatchEvent(
            new CustomEvent("info-person",{
                detail:{
                    name:this.fname
                }
            }));        
    }
    deletePerson(e){
        console.log("deletePerson en persona-ficha-listado");
        console.log("Se va a borrar la persona de nombre "+this.fname);
        
        this.dispatchEvent(
            new CustomEvent("delete-person",{
                    detail: {
                        name: this.fname
                    }
                }
            )
        );
    }

    
 
    
}

customElements.define("persona-ficha-listado", PersonaFichaListado);