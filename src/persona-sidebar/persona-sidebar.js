import { LitElement, html } from "lit-element";

class PersonaSidebar extends LitElement{

    static get properties(){
        return{
           peopleStats: {type: Object},
        };
    }

    constructor(){
        super();
        this.peopleStats = {};
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <aside>
            <section>
                <div>
                    Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas
                </div>
                <div class="mt-2">
                    <label for="customRange3" class="form-label">Años en la empresa</label>
                    <input type="range" class="form-range" value="${this.peopleStats.maxYearsInCompany}" max="${this.peopleStats.maxYearsInCompany}" step="1" @input="${this.updateMaxYearsInCompany}">       
                    <span>Años: ${this.peopleStats.maxYearsInCompany}</span>
                </div>
                <div class="mt-3">
                    <button @click=${this.newPerson} class="w-100 btn bg-success" style="font-size: 50px"><strong>+</strong></button>
                </div>
            </section>
        </aside>                 
        `;
    }

    updated(changedProperties){
        console.log("updated")        
        if (changedProperties.has("peopleStats")){
            console.log("se ha actualizado la propiedad peopleStats en persona-sidebar")            
        }
    }   
        
    

    newPerson(e){
        console.log("newPerson en persona-sidebar");
        console.log("Se va a crear una nueva persona");
        this.dispatchEvent(new CustomEvent("new-person",{}));
    }

    updateMaxYearsInCompany(e){
        console.log("updateMaxYearsInCompany")
        console.log("El range vale "+e.target.value)
        this.dispatchEvent(new CustomEvent("updated-max-years-filter", {
            detail: {
                maxYearsInCompany: e.target.value
            }
        }))
              

    }
}

customElements.define("persona-sidebar", PersonaSidebar);