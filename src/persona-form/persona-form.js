import { LitElement, html } from "lit-element";

import "../persona-header/persona-header.js";
import "../persona-main/persona-main.js";
import "../persona-footer/persona-footer.js";
import "../persona-sidebar/persona-sidebar.js";

class PersonaFrom extends LitElement{

    static get properties(){
        return{
            person: {type:Object},
            editingPerson: {type:Boolean}            
        };
    }

    constructor(){
        super();
        this.resetFormData();
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <div>
            <form>
                <div class="form-group">
                    <label>Nombre completo: </label>
                    <input @input="${this.updateName}" 
                        .value="${this.person.name}" 
                        ?disabled="${this.editingPerson}" 
                        type="text" class="form-control" placeholder="Nombre Completo" />
                </div>                     
                <div class="form-group">  
                    <label>Perfil: </label>              
                    <textarea @input="${this.updateProfile}" .value="${this.person.profile} "class="form-control" placeholder="Perfil" rows="5"></textarea>
                </div> 
                <div class="form-group">
                    <label>Años en la empresa: </label>
                    <input @input="${this.updateYearsInCompany}" .value="${this.person.yearsInCompany} "type="text" class="form-control" placeholder="Años en la empresa" />
                </div> 
                <button @click="${this.goBack}" class="btn btn-success"><strong>Atras</strong></button>
                <button @click="${this.storePerson}"class="btn btn-primary"><strong>Guardar</strong></button>                
            </form>
        </div>
        `;
    }

    updateName(e){
        console.log("updateName");
        console.log("Actualizando el valor de la propiedad name de person con valor "+e.target.value);
        this.person.name=e.target.value;
    }
    updateProfile(e){
        console.log("updateProfile");
        console.log("Actualizando el valor de la propiedad profile de person con valor "+e.target.value);
        this.person.profile=e.target.value;
    }
    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        console.log("Actualizando el valor de la propiedad YearsInCompany de person con valor "+e.target.value);
        this.person.yearsInCompany=e.target.value;
    }
    goBack(e){
        console.log("goBack");
        console.log("Se ha cerrado el formulario de persona");
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("persona-form-close", {}));
        this.resetFormData();
    }
    storePerson(e){
        console.log("storePerson")
        console.log("Enviamos el formulario")
        e.preventDefault();
        console.log("La propiedad name de person vale "+this.person.name)
        console.log("La propiedad profile de person vale "+this.person.profile)
        console.log("La propiedad yearsInCompany de person vale "+this.person.yearsInCompany)
        this.person.photo = {
            src:"./img/caras-10.png",
            alt:"Persona"
        }
        this.dispatchEvent(new CustomEvent("persona-form-store", {
            detail: {
                person: {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo
                },
                editingPerson: this.editingPerson
            }
        }));

    }

    resetFormData(){
        console.log("resetFormData");
        
        this.person= {};
        this.person.name="";
        this.person.profile="";
        this.person.yearsInCompany="";

        this.editingPerson=false;
    }
}

customElements.define("persona-form", PersonaFrom);